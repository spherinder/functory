from typeclass import Monad, Monoid


class Log(Monad):

    def __init__(self, value, logMessage=None):
        if not logMessage:
            super().__init__(tuple(value))
        else:
            super().__init__((value, logMessage))

    def __str__(self):
        return "Log" + str(self.value)

    def map(self, func):
        value, log = self.value
        newValue = func(value)
        return Log((newValue, log))

    def amap(self, functorValue):
        functorContent, log = functorValue.value
        newValue = self.value[0](functorContent)
        return Log((newValue, log))

    def bind(self, func):
        startValue, startLog = self.value
        newValue, newLog = func(startValue).value
        return Log((newValue, startLog + newLog))

    @classmethod
    def unit(cls, value):
        return Log((value, cls.logType.zero()))

    def getResult(self):
        return self.value[0]

    def getLog(self):
        return self.value[1]

    def __eq__(self, other):
        if isinstance(other, Log):
            return super().__eq__(other)
        else:
            raise TypeError("Can't compare two different types.")


class NumberWriter(Log):
    logType = int


class StringWriter(Log):
    logType = str


class StrictListWriter(Log):
    logType = list
