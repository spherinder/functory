from typeclass import (
    Monad,
    Monoid,
    Functor,
    Applicative,
    Foldable,
    Traversable,
    Semigroup,
    Container
)
from Maybe import (Maybe)
from Result import (Result)
from StrictList import (StrictList, concat, map, range)
