from Function import curry
from operator import eq, contains, mod

# Effects
seq = curry(lambda _, x: x)
do = curry(lambda effect, x: seq(effect(x), x))
log = do(lambda x: print("log", x))

# Combinators
identity = curry(lambda x: x)
always = curry(lambda x, _: x)
flip = curry(lambda f, x, y: f(y, x))
ifPredicate = curry(lambda condA, condB, f, x: (f(x) and condA(x)) or condB(x))
apply = curry(lambda x, f: f(x))
applyStar = curry(lambda lst, f: f(*lst))
applyKwargs = curry(lambda dct, f: f(**dct))

# Typeclasses
plus = curry(lambda x, y: x + y)
map = curry(lambda f, x: x.map(f))
amap = curry(lambda ff, fa: ff.amap(fa))
lift2 = curry(lambda f, fa, fb: fa.map(f).amap(fb))
lift3 = curry(lambda f, fa, fb, fc: fa.map(f).amap(fb).amap(fc))
lift4 = curry(lambda f, fa, fb, fc, fd: fa.map(f).amap(fb).amap(fc).amap(fd))
bind = curry(lambda ma, f: ma.bind(f))

# Better defaults
attr = curry(lambda string, obj: getattr(obj, string))
elem = flip(contains)
eq = curry(lambda x, y: x == y)
modulo = flip(mod)
