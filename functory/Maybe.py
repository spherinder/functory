from typeclass import Traversable, Monad, Applicative, Monoid


class Maybe(Monad, Traversable):
    def __init__(self, value):
        raise NotImplementedError("Can not create objects of \
        type Maybe: use Just or Nothing.")

    def __eq__(self, other):
        if not isinstance(other, Maybe):
            raise TypeError("Can not compare two different types.")

    @staticmethod
    def unit(value):
        return Just(value)

    @staticmethod
    def zero():
        return Nothing

    def sequence(self):
        if not isinstance(self.value, Applicative):
            raise TypeError("The inner value must be an Applicative instance.")


class Just(Maybe):

    def __init__(self, value):
        super(Maybe, self).__init__(value)

    def __str__(self):
        return "Just " + str(self.value)

    def __eq__(self, other):
        super().__eq__(other)
        if isinstance(other, _Nothing):
            return False
        else:
            return self.value == other.value

    def __ne__(self, other):
        return not self.__eq__(other)

    def map(self, func):
        return Just(func(self.value))

    def amap(self, functorValue):
        return functorValue.map(self.value)

    def bind(self, func):
        res = func(self.value)
        if isinstance(res, Maybe):
            return res
        else:
            raise TypeError("The bound function must return a Maybe instance.")

    def plus(self, other):
        if other == Nothing:
            return self
        else:
            return Just(self.value + other.value)

    def foldl(self, z, func):
        return func(z, self.value)

    def sequence(self, unit):
        super().sequence()
        return self.value.map(Just)


class _Nothing(Maybe):
    def __init__(self):
        super(Maybe, self).__init__(None)

    def __str__(self):
        return "Nothing"

    def __eq__(self, other):
        super().__eq__(other)
        return isinstance(other, _Nothing)

    def __ne__(self, other):
        return not self.__eq__(other)

    map, amap, bind = [lambda self, _: self] * 3

    def plus(self, other):
        return other

    def foldl(self, z, _):
        return z

    def sequence(self, unit):
        super().sequence()
        return unit(self)


Nothing = _Nothing()


class First(Monoid):
    def __init__(self, value):
        if isinstance(value, Maybe):
            super().__init__(value)
        else:
            raise TypeError

    def __str__(self):
        return str(self.value)

    @staticmethod
    def zero():
        return First(Nothing)

    def plus(self, other):
        if isinstance(self.value, Just):
            return self
        else:
            return other


class Last(Monoid):
    def __init__(self, value):
        if isinstance(value, Maybe):
            super().__init__(value)
        else:
            raise TypeError("")

    def __str__(self):
        return str(self.value)

    @staticmethod
    def zero():
        return First(Nothing)

    def plus(self, other):
        if isinstance(other.value, Just):
            return other
        else:
            return self
