from Function import curry, Function
from helpers import (
    attr,
    ifPredicate,
    do,
    identity,
    lift2,
    log,
    plus,
)
from StrictList import StrictList, replicate, concat, enum
from List import List
from Maybe import Just, Maybe, Nothing

a = StrictList(2, 3, 4, 0)
b = StrictList(9, 7)
c = StrictList(a, b)
maybs = a.map(lambda x: Just(x) if x != 0 else Nothing)

print("str", c)
print("plus", b, a, b + a)
print("concat", concat(StrictList, c))
print("enum", enum(0, 10))
print("getValue", a.value)
print("bind", a.bind(enum(0)))
print("sequence", c.sequence(StrictList))
print("index a[1]", a[1])
print("slicing", StrictList(2, 3, 5, 6, 7, 3)[2:6])
print("notequal", a != b, a != 3)
print("repeat", replicate(3, b))
print("sequenceJust", maybs[:-1], maybs[:-1].sequence(Maybe))
print("sequenceNothing", maybs, maybs.sequence(Maybe))
print("funcbind", plus("Hello ").bind(
    curry(lambda x, y: log(x).upper() + " <= it's uppercase and has a " + y)
)("yo"))
print("if", ifPredicate(log, identity, curry(lambda x: x % 2 == 0), 2))
print("curry", curry(lambda x: curry(lambda y: x + y))(2, 3))


def getNumArgs(func):
    if isinstance(func, Function):
        value = func.value
        return getNumArgs(value)
    else:
        return func.getj


def curry(func):
    def lambdaRecur(argValues, numArgs, f):
        if (numArgs == 0):
            res = f(*argValues)
            if callable(res):
                if isinstance(res, Function):
                    res = res.value
                return lambdaRecur([], res.__code__.co_argcount, res)
            else:
                return res
        else:
            return lambda x: lambdaRecur(argValues + [x], numArgs - 1, f)
    return Function(lambdaRecur([], func.__code__.co_argcount, func))

