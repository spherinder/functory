import functools


def check_finite(func):
    @functools.wraps(func)
    def wrap(self, *args, **kwargs):
        if self.inf:
            raise ValueError("Invalid Operation on Infinite List!")
        return func(self, *args, **kwargs)
    return wrap


class List(object):

    def __init__(self, x, getsize=None, inf=False):
        if callable(x):
            self.func = x
        elif 
        self.cache = dict()
        self.getsize = getsize
        self.inf = inf
        self.size = None

    def __len__(self):
        if self.inf:
            return 0
        if self.size:
            return self.size
        if self.getsize:
            self.size = self.getsize()
            return self.size
        # try to evaluate every element until out of bound
        i = max(self.cache.keys(), default=0)
        while True:
            try:
                self[i]
            except IndexError:
                break
            i += 1
        self.size = i
        return self.size

    def __contains__(self, item):
        return self.elem_index(item) != -1

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop, step = key.start or 0, key.stop or len(
                self), key.step or 1
            if not self.inf and stop:
                size = (stop - start) // step + 1
                return List(
                    lambda _, i: self[start + i * step],
                    lambda: size
                )

            # when stop is not provided and self is infinite, result is infinite
            return List(lambda _, i: self[start + i * step], inf=True)
        elif isinstance(key, int):
            if key in self.cache:
                return self.cache[key]
            self._evaluate(key)
            return self.cache[key]
        else:
            raise KeyError("Index can only be slice or integer!")

    def __str__(self):
        return str(self.to_list())

    def _evaluate(self, i):
        self.cache[i] = self.func(self, i)

    @check_finite
    def __add__(self, other):
        def getitem(_, i):
            try:
                return self[i]
            except IndexError:
                return other[i - len(self)]

        return List(
            getitem,
            lambda: len(self) + len(other),
            self.inf or other.inf)
