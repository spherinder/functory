from typeclass import Monad


class Function(Monad):

    def __call__(self, *args):
        value = self.value
        for a in args:
            try:
                value = value(a)
            except TypeError:
                raise TypeError(
                    "Too many arguments supplied to curried function.")
        if callable(value):
            return Function(value)
        else:
            return value

    def map(self, func):
        return Function(lambda x: func(self.value(x)))

    def amap(self, functorValue):
        return Function(lambda x: self.value(x, functorValue(x)))

    def bind(self, func):
        return Function(lambda x: func(self.value(x), x))

    @staticmethod
    def unit(value):
        return Function(lambda _: value)


def curry(func):
    def lambdaRecur(argValues, numArgs, f):
        if (numArgs == 0):
            res = f(*argValues)
            if callable(res):
                if isinstance(res, Function):
                    res = res.value
                return lambdaRecur([], res.__code__.co_argcount, res)
            else:
                return res
        else:
            return lambda x: lambdaRecur(argValues + [x], numArgs - 1, f)
    return Function(lambdaRecur([], func.__code__.co_argcount, func))
