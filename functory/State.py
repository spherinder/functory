from typeclass import Monad


class State(Monad):

    def map(self, func):

        @State
        def newState(state):
            result, st = self(state)
            return (func(result), state)
        return newState

    def amap(self, functorValue):
        @State
        def newState(state):
            func = self.getResult(state)
            value = functorValue.getResult(state)
            return (func(value), state)
        return newState

    def bind(self, func):
        @State
        def newState(state):
            result, st = self(state)
            if isinstance(st, State):
                return func(result, st)
            else:
                raise TypeError(
                    "The bound function must return a State value.")
        return newState

    @staticmethod
    def unit(value):
        return State(lambda state: (value, state))

    def getResult(self, state):
        return self.value(state)[0]

    def getState(self, state):
        return self.value(state)[1]

    def __call__(self, state):
        return self.value(state)

    def __eq__(self, other):
        raise TypeError("State: Can't compare functions for equality.")
