import builtins
from functools import reduce
from Function import curry
from helpers import flip, lift2
from typeclass import Monad, Traversable


class StrictList(list, Monad, Traversable):

    def __init__(self, *values):
        self.value = self  # Overwritten by list
        super().__init__(values)

    def __str__(self):
        return "StrictList(" + ", ".join(self.map(str)) + ")"

    # Monad
    def map(self, func):
        return StrictList(*builtins.map(func, self))

    @staticmethod
    def unit(value):
        return StrictList(value)

    def amap(self, functorValue):
        return concat(StrictList, self.map(functorValue.map))

    def bind(self, f):
        return concat(StrictList, self.map(f))

    # Monoid
    @staticmethod
    def zero():
        return StrictList()

    def plus(self, other):
        if not isinstance(other, StrictList):
            raise TypeError("Can not concatenate StrictList with non-StrictList type.")
        else:
            return StrictList(*(list.__add__(self, other)))

    # Traversable
    def foldl(self, z, func):
        return reduce(func, self.value, z)

    def sequence(self, innerType):
        return reduce(lift2(flip(append)), self, innerType.unit(StrictList()))

    # Overwritten
    def __eq__(self, other):
        if isinstance(other, StrictList):
            return super().__eq__(other)
        else:
            raise TypeError("Can not compare StrictList with non-StrictList type.")

    def __ne__(self, other):
        return not isinstance(other, StrictList) or super().__ne__(other)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return StrictList(*super().__getitem__(key))
        else:
            return super().__getitem__(key)

    __add__ = plus
    __rmul__ = map


concat = curry(lambda innerType, xs: sum(xs, innerType.zero()))
append = curry(lambda x, xs: xs + StrictList(x))
cons = curry(lambda x, xs: StrictList(x) + xs)
getItem = curry(lambda idx, xs: xs[idx])
replicate = curry(lambda n, xs: xs * n)
take = curry(lambda n, xs: xs[n])
enum = curry(lambda start, end: StrictList(*range(start, end)))
