from abc import abstractmethod, ABCMeta


class Container(metaclass=ABCMeta):
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return self.value == other.value


class Functor(Container):
    @abstractmethod
    def map(self, func): pass
    __mul__ = lambda self, func: self.map(func)


class Apply(Container):
    @abstractmethod
    def amap(self, functorValue): pass


class Applicative(Functor, Apply):
    @staticmethod
    @abstractmethod
    def unit(value): pass


class Monad(Applicative):
    @abstractmethod
    def bind(self, func): pass
    __rshift__ = lambda self, func: self.bind(func)


class Semigroup(Container):
    @abstractmethod
    def plus(self, other): pass

    def __add__(self, other):
        res = self.plus(other)
        if isinstance(res, Semigroup):
            return res
        else:
            raise TypeError("Operator '+' must return a Semigroup instance.")


class Monoid(Semigroup):
    @staticmethod
    @abstractmethod
    def zero(): pass


class Foldable(Monoid):
    @abstractmethod
    def foldl(self, z, func): pass


class Traversable(Applicative, Foldable):
    @abstractmethod
    def sequence(self): pass


class Container2():
    def __init__(self, value, valueTwo):
        super().__init__(value)
        self.valueTwo = valueTwo

    def __eq__(self, other):
        return self.value == other.value and self.valueTwo == other.valueTwo


class Bifunctor(Functor):
    @abstractmethod
    def bimap(self, f, g): pass


class ContraFunctor(Container):
    @abstractmethod
    def contramap(self, f): pass


class Profunctor(Functor, ContraFunctor):
    @abstractmethod
    def dimap(self, f, g): pass


class Comonad(Functor):
    @abstractmethod
    def extract(self): pass
    @abstractmethod
    def duplicate(self): pass


class Category(Container):
    @staticmethod
    @abstractmethod
    def identity(x): pass
    @staticmethod
    @abstractmethod
    def compose(f, g): pass


# Takes a function from b -> a
class Dual(Category):

    @staticmethod
    def identity(x):
        return Dual(lambda x: x.identity())

    @staticmethod
    def compose(cat, f, g):
        return Dual(cat.compose(f, g))
