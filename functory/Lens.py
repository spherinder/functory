from typeclass import Functor
from Function import curry
from helpers import attr, plus, always


class Const(Functor):
    def map(self, _):
        return self


class Identity(Functor):
    def map(self, func):
        return Identity(func(self.value))


@curry
def view(lens):
    return lens(Const).map(attr("value"))


@curry
def over(lens, f):
    return lens(f.map(Identity)).map(attr("value"))


@curry
def set(lens, f):
    return over(lens)


@curry
def dictLens(key, toFunctor, dct):
    def set(x):
        res = dict(dct)
        res[key] = x
        return res
    return toFunctor(dct[key]).map(set)

print(over(dictLens("a"), plus(2), {"a": 2}))
